/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fps;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @param <T> 
 * @author Marcin
 */
public class CheckList<T> {
    private ArrayList<T> storage = new ArrayList<T>();
    //private Random rand = new Random();
    /**
     *
     */
    public int actual = 0;
    /**
     *
     * @param item
     */
    public void add(T item) {
        storage.add(item); 
    }
    
    /**
     *
     */
    public void next() {
        if(actual < storage.size()-1) {
            ++actual;
        }else {
            actual = 0;
        }
    }
    
    /**
     *
     * @param items
     */
    public void add(List<T> items) {
        storage.addAll(items);
    }
    /**
     *
     * @return
     */
    public T select() {
        return storage.get(actual);
    }
    
}
