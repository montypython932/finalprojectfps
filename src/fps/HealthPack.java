
package fps;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.Random;


/**
 *
 * @author Phil
 */
public class HealthPack extends Item implements AnimEventListener {
    
    private Node health, node;
    private AnimControl control;
    private AnimChannel channel;
    private Random rand = new Random();
    private CharacterControl healthC;
    
    /**
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param z z coordinate
     * @param he The spatial in this case health pack
     * @param name The spatials name
     */
    public void createMob(float x, float y, float z, Spatial he, String name){
        this.health = (Node) he.clone();
        health.getChild(0).setName(name);
        health.setLocalTranslation(x, y, z);
        //.. and add physics
        CapsuleCollisionShape healShape = new CapsuleCollisionShape(1.0f, 1.0f);
        healthC = new CharacterControl(healShape, 0f);
        node = new Node();
        node.attachChild(health);
        node.addControl(healthC); 
    }
    
    
    /**
     *
     * @param nnode Node to which attach this
     */
    public void attachTo(Node nnode) {
        nnode.attachChild(this.node);
    }
    
    
    /**
     *
     * @param distance Distance from player
     * @return If close enough to heal or not
     */
    public boolean hitHealthPack(double distance) {
        if (distance <= 10) {
            writeToFile("Healing");
            return true;
        } else {
            return false;
        }
    }
    
    /**
     *
     * @param p Targets location
     * @return distance
     */
    public double calculateDist(Vector3f p) {
        double a = healthC.getPhysicsLocation().x - p.x;
        double b = healthC.getPhysicsLocation().z - p.z;
        double dist = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        return dist;
    }
    
    /**
     *
     * @return CharacterControl
     */
    public CharacterControl getCharacterControl() {
        return healthC;
    }

    /**
     *
     * @param control Controls the animation (ie. walk, stand, dead)
     * @param channel The animation channel
     * @param animName The name of the animation (ie. walk, stand, dead)
     */
    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param control Controls the animation (ie. walk, stand, dead)
     * @param channel The animation channel
     * @param animName The name of the animation (ie. walk, stand, dead)
     */
    public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
