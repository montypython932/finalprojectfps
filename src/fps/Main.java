/*
 * Main class
 */
package fps;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.BloomFilter;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Quad;
import com.jme3.scene.shape.Sphere;
import com.jme3.terrain.geomipmap.TerrainLodControl;
import com.jme3.util.SkyFactory;
import java.util.Random;

/**
 *
 * @author Marcin
 * Original Code from http://www.mediafire.com/download/xak5haoz57vra0t/SFfps.rar
 * Edited by Phil
 */
public class Main extends SimpleApplication implements ActionListener, AnimEventListener {

    //Spatials
    private Spatial terrain;
    private Spatial mon, heal;
    private Node gun;
    private Node shootable;
    private Node pickups;
    //physics and control
    private BulletAppState bulletAppState;
    private RigidBodyControl landscape;
    private CharacterControl player;
    //walking variables
    private Vector3f walkDirection = new Vector3f();
    private boolean left = false, right = false, up = false, down = false;
    //Animation stuff
    private AnimChannel channel;
    private AnimControl control;
    private boolean moving = false;
    private boolean shooting = false;
    //temp vectors
    private Vector3f camDir = new Vector3f();
    private Vector3f camLeft = new Vector3f();
    private Geometry mark;
    //particle emitters
    private ParticleEmitter fire;
    private ParticleEmitter blood;
    private MonsterMob mobs[];
    private HealthPack heals[];
    private Geometry geom;
    private int hp = 250;
    private int points = 0;
    private BitmapText hudText;
    private int mobNum;
    private boolean dead = false;
    private DirectionalLight dl;
    private AudioNode background;
    private AudioNode gunSound;
    Random rand = new Random();

    //just main method
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }

    //Pysics
    /**
     * Initializes the Application
     */
    @Override
    public void simpleInitApp() {
        //postprocessing - adding simple bloom
        FilterPostProcessor fpp=new FilterPostProcessor(assetManager);
        BloomFilter bloom=new BloomFilter();
        fpp.addFilter(bloom);
        viewPort.addProcessor(fpp);
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        viewPort.setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
        flyCam.setMoveSpeed(100);
        setUpKeys();
        setUpLight();
        
        // We load the terrain
        terrain = assetManager.loadModel("Scenes/WorldMap.j3o");
        terrain.setLocalScale(3f);
        // We set up collision detection for the scene by creating a
        // compound collision shape and a static RigidBodyControl with mass zero.
        CollisionShape sceneShape = CollisionShapeFactory.createMeshShape((Node) terrain);
        landscape = new RigidBodyControl(sceneShape, 0);
        terrain.addControl(landscape);

        // We set up collision detection for the player by creating
        // a capsule collision shape and a CharacterControl.
        CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(9.0f, 6f, 1);
        player = new CharacterControl(capsuleShape, 0.09f);
        player.setJumpSpeed(120);
        player.setFallSpeed(130);
        player.setGravity(250);
        player.setPhysicsLocation(new Vector3f(0, 20, -260));

        //load gun and set up animation control/channel
        gun = (Node) assetManager.loadModel("Models/Cube.mesh.xml");
        gun.scale(4f, 4, 4f);
        control = gun.getControl(AnimControl.class);
        control.addListener(this);
        channel = control.createChannel();
        channel.setAnim("walk");
        channel.setLoopMode(LoopMode.DontLoop);
        createMobs();
        createHeals();
        mobNum = mobs.length;
        drawText();

        // We attach the scene and the player to the rootnode and the physics space,
        // to make them appear in the game world.
        rootNode.attachChild(terrain);
        rootNode.attachChild(gun);

        rootNode.attachChild(SkyFactory.createSky(assetManager, "Textures/ANGMAP11.dds", false));
        bulletAppState.getPhysicsSpace().add(landscape);
        bulletAppState.getPhysicsSpace().add(player);
        //level od detail control
        TerrainLodControl lodControl = rootNode.getChild("terrain-WorldMap").getControl(TerrainLodControl.class);
        if (lodControl != null) {
            System.out.println("yeah" + rootNode.getChild("WorldMap"));
            lodControl.setCamera(getCamera());
        } else {
            System.out.println("nulllll");
        }

        //init '+' in the middle of screen to help aim
        initCrossHairs();
        initMark();
        initAudio();
        setUpParticles();
        Quad mesh = new Quad(settings.getWidth(), settings.getHeight());
        geom = new Geometry("A shape", mesh); // wrap shape into geometry
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");   // create material
        mat.setTexture("ColorMap", assetManager.loadTexture("Textures/red.png"));
        mat.setColor("Color", ColorRGBA.Red);
        mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
        
        geom.setMaterial(mat);                         // assign material to geometry
        // if you want, transform (move, rotate, scale) the geometry.
        guiNode.attachChild(geom);

    }

    /*
     * Creates and spawns the monsters 
     */
    private void createMobs() {
        mon = assetManager.loadModel("Models/monster/Cube.mesh.xml");
        mon.scale(4f, 4f, 4f);
        shootable = new Node();
        mobs = new MonsterMob[20];
        
        for (int i = 0; i < mobs.length; i++) {
            float randomX = rand.nextFloat() * 500f;
            if (randomX <= 250f) {
                randomX = -randomX;
            }
            float randomZ = rand.nextFloat() * 500f;
            if (randomZ <= 250f) {
                randomZ = -randomZ;
            }
            mobs[i] = new MonsterMob();
            mobs[i].createMob(randomX, 7.3f, randomZ, mon, "mob" + i);
            mobs[i].attachTo(shootable);
            bulletAppState.getPhysicsSpace().add(mobs[i].getCharacterControl());
        }
        rootNode.attachChild(shootable);
    }

    /*
     * Creates and spawns healthpacks
     */
    private void createHeals() {
        heal = assetManager.loadModel("Models/wrench/wrench.mesh.xml");
        heal.scale(40f, 40f, 40f);
        pickups = new Node();
        heals = new HealthPack[5];
        
        for (int i = 0; i < heals.length; i++) {
            float randomX = rand.nextFloat() * 500f;
            if (randomX <= 250f) {
                randomX = -randomX;
            }
            float randomZ = rand.nextFloat() * 500f;
            if (randomZ <= 250f) {
                randomZ = -randomZ;
            }
            heals[i] = new HealthPack();
            heals[i].createMob(randomX, 3.0f, randomZ, heal, "healthPack" + i);
            heals[i].attachTo(pickups);
        }
        rootNode.attachChild(pickups);
    }
    
    /**
     * Displays HUD with health, enemies remaining and points
     */
    public void drawText() {
        hudText = new BitmapText(guiFont, false);
        hudText.setSize(guiFont.getCharSet().getRenderedSize());      // font size
        hudText.setColor(ColorRGBA.Yellow);                             // font color
        hudText.setText("HEALTH: " + hp + "    ENEMIES: " + mobNum + "    POINTS: " + points);             // the text
        hudText.setLocalTranslation(250, hudText.getLineHeight(), 1.0f); // position
        //hudText.rotate(0, 50, 0);
        guiNode.attachChild(hudText);
    }

    /*
     * Creates particles for blood and fire (as in shooting)
     */
    private void setUpParticles() {
        //particle when player fire from weapon
        fire = new ParticleEmitter("Emitter", ParticleMesh.Type.Triangle, 20);
        Material mat_red = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        mat_red.setTexture("Texture", assetManager.loadTexture("Textures/flame.png"));
        fire.setMaterial(mat_red);
        fire.setImagesX(2);
        fire.setImagesY(2); // 2x2 texture animation
        fire.setEndColor(new ColorRGBA(0.0f, 0.8f, 1.0f, 1f));
        fire.setStartColor(new ColorRGBA(0f, 0f, 1.0f, 0.5f));
        fire.getParticleInfluencer().setInitialVelocity(player.getWalkDirection().mult(5));
        fire.setStartSize(0.2f);
        fire.setEndSize(1.0f);
        fire.setGravity(0, 0, 0);
        fire.setLowLife(0.03f);
        fire.setHighLife(0.08f);
        fire.getParticleInfluencer().setVelocityVariation(0.1f);
        fire.setLocalTranslation(-3.7f, -0.13f, -1.95f);

        //blood particles (when mob got hitted)
        blood = new ParticleEmitter("Emitter", ParticleMesh.Type.Triangle, 90);
        Material mat_red2 = new Material(assetManager,
                "Common/MatDefs/Misc/Particle.j3md");
        mat_red2.setTexture("Texture", assetManager.loadTexture(
                "Textures/flame.png"));
        blood.setMaterial(mat_red2);
        blood.setImagesX(2);
        blood.setImagesY(2); // 2x2 texture animation
        blood.setEndColor(new ColorRGBA(1.0f, 0.0f, 0.0f, 1f));
        blood.setStartColor(new ColorRGBA(1.0f, 0f, 0.0f, 1f));
        blood.getParticleInfluencer().setInitialVelocity(new Vector3f(0, -15, 0));
        blood.setStartSize(0.1f);
        blood.setEndSize(2.5f);
        blood.setGravity(0, 20, 0);
        blood.setLowLife(0.5f);
        blood.setHighLife(1.5f);
        blood.getParticleInfluencer().setVelocityVariation(0.3f);
        blood.setLocalTranslation(0, 0, 0);
    }

    /*
     * Sets up the ambient light so we can see
    */
    private void setUpLight() {
        AmbientLight al = new AmbientLight();
        al.setColor(ColorRGBA.White.mult(1.3f));
        rootNode.addLight(al);
        dl = new DirectionalLight();
        dl.setColor(ColorRGBA.White);
        dl.setDirection(new Vector3f(2.8f, -2.8f, -2.8f).normalizeLocal());
        rootNode.addLight(dl);
    }
     
    /**
     * A red ball that marks the last spot that was "hit" by the "shot".
     */
    protected void initMark() {
        Sphere sphere = new Sphere(30, 30, 0.2f);
        mark = new Geometry("BOOM!", sphere);
        Material mark_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mark_mat.setColor("Color", ColorRGBA.Red);
        mark.setMaterial(mark_mat);
    }

 
    /**
     * Initializes and displays crosshairs
     */
    protected void initCrossHairs() {
        setDisplayStatView(false);
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        BitmapText ch = new BitmapText(guiFont, false);
        ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
        ch.setText("."); // crosshairs
        ch.setLocalTranslation( // center
        settings.getWidth() / 2 - ch.getLineWidth() / 2, settings.getHeight() / 2 + ch.getLineHeight() / 2, 0);
        guiNode.attachChild(ch);
    }

    
    /*sets up keybindings
     * 
     */
    private void setUpKeys() {
        inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("Shoot", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        inputManager.addListener(this, "Left");
        inputManager.addListener(this, "Right");
        inputManager.addListener(this, "Up");
        inputManager.addListener(this, "Down");
        inputManager.addListener(this, "Jump");
        inputManager.addListener(this, "Shoot");
    }

    /*
     * check for user input
     */
    public void onAction(String binding, boolean isPressed, float tpf) {

        moving = isPressed;

        if (binding.equals("Left")) {
            left = isPressed;

        } else if (binding.equals("Right")) {
            right = isPressed;
        } else if (binding.equals("Up")) {
            up = isPressed;
        } else if (binding.equals("Down")) {
            down = isPressed;
        } else if (binding.equals("Shoot")) {
            if (isPressed) {
                gunSound.playInstance();
                shooting = true;

            } else {
                fire.setParticlesPerSec(1500);
                gun.attachChild(fire);
                fire.emitAllParticles();
                // 1. Reset results list.
                CollisionResults results = new CollisionResults();
                // 2. Aim the ray from cam loc to cam direction.
                Ray ray = new Ray(cam.getLocation(), cam.getDirection());
                // 3. Collect intersections between Ray and Shootables in results list.
                shootable.collideWith(ray, results);
                if (results.size() > 0) {
                    // The closest collision point is what was truly hit:
                    CollisionResult closest = results.getClosestCollision();

                    //mark.setLocalTranslation(closest.getContactPoint());
                    Geometry g = closest.getGeometry();
                    //shootable.detachChild(g.getParent());

                    //make some blood
                    blood.setLocalTranslation(closest.getContactPoint());
                    blood.setParticlesPerSec(1500);
                    shootable.attachChild(blood);
                    blood.emitAllParticles();

                    //check if any of mobs got hitted
                
                    for (int i = 0; i < mobs.length; i++) {
                        if (g.getName().equals("mob" + i) && !mobs[i].isDead()) {
                            mobs[i].getHitted();
                            if (mobs[i].getHP() <= 0) {
                                --mobNum;
                                points = points + (100 * (250%hp));
                             }
                        }
                    }
                }
            }
        } else if (binding.equals("Jump")) {
            moving = true;
            if (isPressed) {
                player.jump();
            }
        }
    }

    /**
     * We check in which direction the player is walking by interpreting the
     * camera direction forward (camDir) and to the side (camLeft).
     * @param tpf 
     */
    @Override
    public void simpleUpdate(float tpf) {
        guiNode.detachChild(geom);
        //player cam and walkDirection
        camDir.set(cam.getDirection()).multLocal(0.6f);
        camLeft.set(cam.getLeft()).multLocal(0.4f);
        walkDirection.set(0, 0, 0);
        if (left) {
            walkDirection.addLocal(camLeft);
        }
        if (right) {
            walkDirection.addLocal(camLeft.negate());
        }
        if (up) {
            walkDirection.addLocal(camDir);
        }
        if (down) {
            walkDirection.addLocal(camDir.negate());
        }

        player.setWalkDirection(walkDirection);
        cam.setLocation(player.getPhysicsLocation());

        //gun rotation
        gun.setLocalRotation(cam.getRotation());
        gun.rotate(0, 90f, 0);
        gun.setLocalTranslation(player.getPhysicsLocation());

        //movement
        for (int i = 0; i < mobs.length; i++) {
            mobs[i].setMoving(player.getPhysicsLocation());
            if (mobs[i].isAttacking()) {
                guiNode.attachChild(geom);
            }
        }
        for (int i = 0; i < heals.length; i++) {
            if (heals[i] != null && heals[i].hitHealthPack(heals[i].calculateDist(player.getPhysicsLocation())) == true) {
                hp = hp + 100;
                heals[i] = null;
            }
        }
        if (guiNode.hasChild(geom) && hp > 0) {
            hp--;
        }
        //manage particles
        fire.setParticlesPerSec(0);
        if (fire.getNumVisibleParticles() == 0) {
            fire.killAllParticles();
            fire.removeFromParent();

        }
        blood.setParticlesPerSec(0);
        if (blood.getNumVisibleParticles() == 0) {
            blood.killAllParticles();
            blood.removeFromParent();

        }
        hudText.setText("HEALTH: " + hp + "    ENEMIES: " + mobNum + "    POINTS: " + points);             // the text
        if (hp == 0) {
            dead = true;
        }
        if (dead) {
            walkDirection = new Vector3f(0, 0, 0);
            shooting = false;
            player.setEnabled(paused);
            rootNode.detachChild(gun);
            cam.lookAt(new Vector3f(45, 50, 45), new Vector3f(0, 1, 0));
        }
    }

    /**
     *
     * @return GUI node with HUD
     */
    public Node getGui() {
        return guiNode;
    }
    
    /*
     * Initializes game audio
     */
    private void initAudio() {
        
    //Plays background music
    background = new AudioNode(assetManager, "Sounds/action.ogg", true);
    background.setPositional(true);   
    background.setVolume(3);
    rootNode.attachChild(background);
    background.play();
    
    //Play gunshot sound when needed
    gunSound = new AudioNode(assetManager, "Sounds/shoot.ogg", false);
    gunSound.setPositional(false);
    gunSound.setLooping(false);
    gunSound.setVolume(2);
    rootNode.attachChild(gun);
  }

    /**
     *
     * @param control Controls the animation (ie. walk, stand, dead)
     * @param channel The animation channel
     * @param animName The name of the animation (ie. walk, stand, dead)
     */
    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
        if (channel == this.channel) {
            if (shooting) {
                shooting = false;
                channel.setAnim("shoot", 0.14f);
                channel.setLoopMode(LoopMode.DontLoop);
                channel.setSpeed(2f);
            } else {
                if (moving) {
                    channel.setAnim("walk", 0.30f);
                    channel.setLoopMode(LoopMode.DontLoop);
                    channel.setSpeed(1f);
                }
            }
        }
    }

    /**
     *
     * @param control Controls the animation (ie. walk, stand, dead)
     * @param channel The animation channel
     * @param animName The name of the animation (ie. walk, stand, dead)
     */
    public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
        //unused
    }
}
