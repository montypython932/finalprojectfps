
package fps;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.Random;

/**
 *
 * @author Marcin
 */
public class MonsterMob extends Item implements AnimEventListener {

    private Node monster, node;
    private AnimChannel channel;
    private AnimControl control;
    private CharacterControl monsterC;
    private Vector3f dir, temp;
    private static final int WANDER = 0;
    private static final int CHASE = 1;
    private static final int ATTACK = 2;
    private static final int DEAD = 3;
    /**
     *
     */
    public int mode = 0;
    private CheckList<Vector3f> checkPoints = new CheckList<Vector3f>();
    private Vector3f targetPoint;
    private int hp = 1;
    private Random random = new Random();

    /**
     *
     * @param x The x coordinate
     * @param y The y coordinate
     * @param z The z coordinate
     * @param mon The spatial, in this case a monster
     * @param name Name of the Mob
     */
    public void createMob(float x, float y, float z, Spatial mon, String name) {
        this.monster = (Node) mon.clone();
        monster.getChild(0).setName(name);
        monster.setLocalTranslation(x, y, z);
        control = monster.getControl(AnimControl.class);
        control.addListener(this);
        channel = control.createChannel();
        channel.setAnim("run");
        System.out.println(channel.getAnimationName());
        channel.setLoopMode(LoopMode.Loop);
        channel.setSpeed(4f);

        //.. and add physics
        CapsuleCollisionShape monsterShape = new CapsuleCollisionShape(0.1f, 1f);
        monsterC = new CharacterControl(monsterShape, 1f);
        node = new Node();
        node.setLocalTranslation(x, y, z);
        node.attachChild(monster);
        node.addControl(monsterC);
        monster.setLocalTranslation(0, 7, 0);

        //Adds to the list their possible destinations
        for (int i = 0; i < 5; i++) {
            x = random.nextInt(500);
            z = random.nextInt(500);
            checkPoints.add(new Vector3f(x, 0, z));
        }
    }

    //Calculates the direction of the player
    /**
     *
     * @param target Target for calculating direction, usually the player
     * @return The targets location
     */
    public Vector3f calcDir(Vector3f target) {
        temp = new Vector3f(target.x - monsterC.getPhysicsLocation().x, 0, target.z - monsterC.getPhysicsLocation().z);
        return temp;
    }

    
    /**
     * @return weather the monster is attacking or not
     */
    public boolean isAttacking(){
        return channel.getAnimationName().equals("attack");
    }

    
    /**
     * Determines animation if being shot
     */
    public void getHitted() {
        if (hp > 0) {
            channel.setAnim("hitted");
            channel.setLoopMode(LoopMode.DontLoop);
            channel.setSpeed(5);

            hp--;
            if (hp == 0) {
                writeToFile(this.toString() + " is dead!");
                channel.setAnim("dead");
                channel.setLoopMode(LoopMode.DontLoop);
                channel.setSpeed(5);
            }
        }
    }

    /**
     *
     * @return The Mobs Health
     */
    public int getHP() {
        return hp;
    }

    /**
     * Is the mob dead?
     * @return
     */
    public boolean isDead() {
        return mode == DEAD;
    }

    //toggles between modes
    /**
     *
     * @param target The target location for the mob to move towards
     */
    public void setMoving(Vector3f target) {

        if (mode == CHASE) {
            //diretion mob to target
            dir = calcDir(target);

            calculateDist(target);
            setDirection(new Vector3f(0, 0, 0));

            monsterC.setViewDirection(dir);

            if (calculate3DDist(target) >= 15) {
                setDirection(dir.normalize().divide(2));
            }

        } else if (mode == WANDER) {
            targetPoint = checkPoints.select();
            //diretion mob to target
            dir = calcDir(targetPoint);
            setDirection(new Vector3f(0, 0, 0));
            monsterC.setViewDirection(dir);   
            if (calculateDist(targetPoint) >= 10) {
                setDirection(dir.normalize().divide(2));
            } else {
                checkPoints.next();
            }

        } else if (mode == ATTACK) {
            System.out.println("Attacking!!");
        } else if (mode == DEAD) {
            setDirection(new Vector3f(0, 0, 0));
        }
        if (hp == 0) {
            mode = DEAD;
        } else if (calculate3DDist(target) < 80 && calculate3DDist(target) > 25) {
            mode = CHASE;
        } else if (calculate3DDist(target) < 25) {
            mode = ATTACK;
        } else {
            mode = WANDER;
        }
    }

    /**
     *
     * @param nnode The node to which the mob is attached
     */
    public void attachTo(Node nnode) {
        nnode.attachChild(this.node);
    }

    /**
     *
     * @param newMode Mode for the mobs behavior
     */
    public void setMode(int newMode) {
        if (newMode <= 2) {
            this.mode = newMode;
        }
    }

    /**
     *
     * @param dir Direction towards target
     */
    public void setDirection(Vector3f dir) {
        monsterC.setWalkDirection(dir);
    }

    /**
     *
     * @return CharacterControl
     */
    public CharacterControl getCharacterControl() {
        return monsterC;
    }

    //Calculates distance to the player in two dimensions
    /**
     *
     * @param p Location from mod with which distance should be calulated
     * @return Distance
     */
    public double calculateDist(Vector3f p) {
        double a = monsterC.getPhysicsLocation().x - p.x;
        double b = monsterC.getPhysicsLocation().z - p.z;
        double dist = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        return dist;
    }

    //Calculates distance to player in three dimensions
    /**
     *
     * @param p Location from mod with which distance should be calulated
     * @return distance
     */
    public double calculate3DDist(Vector3f p) {
        double a = monsterC.getPhysicsLocation().x - p.x;
        double b = monsterC.getPhysicsLocation().z - p.z;
        double c = monsterC.getPhysicsLocation().y - p.y;
        double dist2 = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2) + Math.pow(c, 2));
        return dist2;
    }

    //Set coordinates of monster
    /**
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param z z coordinate
     */
    public void setCoords(float x, float y, float z) {
        monster.setLocalTranslation(x, y, z);
    }

    //Set rotation of the monster
    /**
     *
     * @param x Face x coordinate
     * @param y Face y coordinate
     * @param z Face z coordinate
     */
    public void setRot(float x, float y, float z) {
        monster.rotate(x, y, z);
    }

    /**
     *
     * @param control Controls the animation (ie. walk, stand, dead)
     * @param channel The animation channel
     * @param animName The name of the animation (ie. walk, stand, dead)
     */
    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
        if (mode != DEAD) {
            if (mode != ATTACK) {
                channel.setAnim("run");
                channel.setLoopMode(LoopMode.Loop);
                channel.setSpeed(4f);
            } else {
                channel.setAnim("attack");
                channel.setLoopMode(LoopMode.Loop);
                channel.setSpeed(2f);
            }
        }
    }

    /**
     *
     * @param control Controls the animation (ie. walk, stand, dead)
     * @param channel The animation channel
     * @param animName The name of the animation (ie. walk, stand, dead)
     */
    public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
    }
}
