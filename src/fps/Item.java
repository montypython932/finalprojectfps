/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fps;


import com.jme3.scene.Node;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Phil
 */
public class Item {
    
    /**
     *
     * @param content String to be appended to file
     */
    public void writeToFile(String content) {
        try {
            File file = new File("output.txt");
            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.newLine();
            bw.close();
            System.out.println("Done");
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
    
}
